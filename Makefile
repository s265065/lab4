all: build

build: main.c linked_list.c linked_list.h
	gcc -o main -ansi -pedantic -Wall -Werror main.c linked_list.c
clean:
	rm -f main

