#include <stdio.h>
#include "linked_list.h"

int main() {
    node* list = NULL;
    int value;

    printf("input numbers:");

    while (scanf("%d", &value) == 1) {
        list_add_front(value, &list);
    }

    printf("sum is: %ld\n", list_sum(list));
    list_free(list);
    return 0;

}
