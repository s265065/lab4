#include "linked_list.h"

#include <stdlib.h>

struct node{
    int data;
    struct node* next;
};

node* list_create(int number){
    node* new_list = malloc(sizeof(number));
    new_list->data = number;
    new_list->next = NULL;
    return new_list;
}

void list_add_front(int number, node **list){
    node* new_list = list_create(number);
    new_list->next = *list;
    *list = new_list;
}

void list_add_back(int number, node* list){
    node* new_node = list_create(number);
    while (list->next != NULL) {
        list = list->next;
    }
    *list->next = *new_node;
}

void list_free(node* list){
    node* for_deleting;
    while (list->next != NULL) {
        for_deleting = list;
        list = list->next;
        free(for_deleting);
    }
}

int list_length(const node* list){
    int length = 0;
    while (list->next != NULL) {
        list = list->next;
        ++length;
    }
    return length;
}

node* list_node_at(node* list, int index){

    while(index>0){
        if (list == NULL) {
            return NULL;
        }

        list = list->next;
        --index;
    }
    return list;
}

int list_get(const node* list, int index){
    const  node* index_node = list_node_at((node*) list, index);
    if (index_node!=NULL){
        return index_node->data;
    }
    return 0;

}

long int list_sum(const node* list){
    int sum = list->data;
    while (list->next != NULL) {
        list = list->next;
        sum+=list->data;
    }
    return sum;
}
